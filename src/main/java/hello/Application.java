package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import no.finn.unleash.DefaultUnleash;
import no.finn.unleash.Unleash;
import no.finn.unleash.util.UnleashConfig;

@SpringBootApplication
@RestController
public class Application {

    UnleashConfig config = UnleashConfig.builder()
            .appName("production")
            .instanceId("4Q3YdG8MG3Zc3sWcnXvM")
            .unleashAPI("https://ps.gl-demo.io/api/v4/feature_flags/unleash/13")
            .build();

    Unleash unleash = new DefaultUnleash(config);

    @RequestMapping("/")
    public String home() {
        String content;

        if(unleash.isEnabled("experimental-feature")) {
            content = "Feature flag Enabled\n Hello from GitLab";
        } else {
            content = "Hello from GitLab";
        }
        return content;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
